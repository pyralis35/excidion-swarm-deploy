'AWS::CloudFormation::Init':
        config:
          files:
            /tmp/bastion_bootstrap.sh:
              source: !If
                - UseAlternativeInitialization
                - !Ref AlternativeInitializationScript
                - !Sub
                  - https://${QSS3BucketName}.${QSS3Region}.amazonaws.com/${QSS3KeyPrefix}scripts/bastion_bootstrap.sh
                  - QSS3Region: !If
                      - GovCloudCondition
                      - s3-us-gov-west-1
                      - s3
              mode: '000550'
              owner: root
              group: root
              authentication: S3AccessCreds